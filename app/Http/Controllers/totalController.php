<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\summary;
use App\account;
use App\categories;
use App\settings;
use Datetime;
use Auth;



class totalController extends Controller
{
      public function totales()
   {    

         $r=(new summaryController)->pass($act='saldo');
      if($r>0 ){  
        $hoy=date('Y-m-d',strtotime('today'));
        $summary = summary::where('created_at','<=',$hoy)->orderBy('id','desc')->get();
        $categories = categories::all();
        $account = account::all();
        $account2 = account::all();
        $divisa = settings::where('name','divisa')->first();
       
       	$response =array();
        $total =array();
        foreach ($account as $a) {
        	$tmp = summary::where('created_at','<=',$hoy)->where('account_id',$a->id)->get();
        	$total[$a->id] = 0;
        	foreach ($tmp as $t) {

        		if($t->type=='out'){
        			$total[$a->id] -= $t->amount;
        		}else{
        		$total[$a->id] += $t->amount;
        		}
        	}
    		$a->setAttribute('total',$total[$a->id]);

        }
        
        $totalfinal=0;
        foreach ($total as $b) {
           
           $totalfinal+=$b;
        }

           //total add
        foreach ($account as $a) {
         $totale = 0;   
            $tmp = summary::where('created_at','>',$hoy)->get();
        
            foreach ($tmp as $t) {

                if($t->type=='add'){
                    $totale += $t->amount;
                }

            }
               $a->setAttribute('totale',$totale);      
        }    
       
         //total out
        foreach ($account as $b) {
         $totals = 0;   
            $tmp = summary::where('created_at','>',$hoy)->get();
         
            foreach ($tmp as $t) {

                if($t->type=='out'){
                    $totals += $t->amount;
                }

            }
               $b->setAttribute('totals',$totals);      
        }   
        //total final
         foreach ($account as $c) {
         $totalf = 0;   
            $tmp = summary::where('created_at','>',$hoy)->get();
         
            foreach ($tmp as $t) {

                if($t->type=='add'){
                    $totalf += $t->amount;
                }
                if($t->type=='out'){
                    $totalf -= $t->amount;
                }

            }
               $c->setAttribute('totalf',$totalf);      
        }    

        $futuro = $account->first();

        $finish=date('Y-m-d',strtotime('today + 30 days'));

        $start = new Datetime($hoy);
        $finish = new Datetime($finish);


       $totalm1 = 0;   
       $totalm2 = 0;
       $totalm3 = 0;  
       $totalm6 = 0;      

         //mes1
         foreach ($account2 as $c) {
         $totalm1 = 0;   
        $tmp = summary::where('created_at','<', $finish)->get();
         
            foreach ($tmp as $t) {

                if($t->type=='add'){
                    $totalm1+= $t->amount;
                }
                if($t->type=='out'){
                    $totalm1 -= $t->amount;
                }

            }
               $c->setAttribute('totalm1',$totalm1);      
        }    

          $logemail = Auth::user()->email;
         
    
        $finish=date('Y-m-d',strtotime('today + 90 days'));
        $finish = new Datetime($finish);
           //mes3
         foreach ($account2 as $c) {
         $totalm3 = 0;   
        $tmp = summary::where('created_at','<',$finish)->get();
         
            foreach ($tmp as $t) {

                if($t->type=='add'){
                    $totalm3+= $t->amount;
                }
                if($t->type=='out'){
                    $totalm3 -= $t->amount;
                }

            }
               $c->setAttribute('totalm3',$totalm3);      
        }    
         


        $finish=date('Y-m-d',strtotime('today + 180 days'));
        $finish = new Datetime($finish);
           //mes6
         foreach ($account2 as $c) {
         $totalm6 = 0;   
        $tmp = summary::where('created_at','<',$finish)->get();
         
            foreach ($tmp as $t) {

                if($t->type=='add'){
                    $totalm6+= $t->amount;
                }
                if($t->type=='out'){
                    $totalm6 -= $t->amount;
                }

            }
               $c->setAttribute('totalm6',$totalm6);      
        }    
        
      
               $account2 = $account2->first();

             

          

             
      
 return view('vendor.adminlte.montos.montos',['summary'=>$account,'divisa'=>$divisa,'totalfinal'=>$totalfinal,'futuro'=>$futuro,'liquidez'=>$account2]);

    }else{
         return view('vendor.adminlte.permission',['summary'=>null]);
    }
   }  
  

}
